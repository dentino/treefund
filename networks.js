const HDWalletProvider = require("@truffle/hdwallet-provider");
var secrets = require("./.secrets.json");

module.exports = {
  networks: {
    development: {
      protocol: "http",
      host: "localhost",
      port: 8545,
      gas: 5000000,
      gasPrice: 5e9,
      networkId: "*"
    },
    ropsten: {
      provider: () =>
        new HDWalletProvider(
          secrets["mnemonic"],
          "http://ropsten.dappnode:8545"
        ),
      networkId: 3,
      gasPrice: 10e9
    }
  }
};
