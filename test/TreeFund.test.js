// test/TreeFund.test.js

const test = require('ava');

const { accounts, contract } = require('@openzeppelin/test-environment');
const [owner] = accounts;
const TreeFund = contract.fromArtifact('TreeFund');

// Should we log emitted events?
const logEvents = false;
const logVerbose = false;

test.before(async (t) => {
  t.context.treeFund = await TreeFund.new({ from: owner });
  // await t.context.treeFund.constructor(owner); // Used for OZ upgradable contracts
});

// #1 -Here we're running check to confirm the deployer is the owner
test('The deployer is the owner...', async (t) => {
  t.is(await t.context.treeFund.owner(), owner);
});

// #2 - Here we confirm that the tree State is 0
// This test is to confirm that totalLeafTime is set to 0 after contract creation
// all leaves created by constructor should have 0 total time added to treeState
// run as serial because other tests were updating state before this completed
test("Tree State shouldn't be set with constructor...", async (t) => {
  const treeState = await t.context.treeFund.treeState();
  t.is(treeState['words'][0], 0);
});

// #3 - Here we test that leaves can be added and retrieved
test('Leaves can be added & retrieved...', async (t) => {
  // get tree state before leaf is added
  const treeStateBefore = await t.context.treeFund.treeState();
  const ESBefore = treeStateBefore['words'][0];

  // add new leaf, random address
  const addedLeaf = await t.context.treeFund.addLeaf(
    '0x8fd00f170fdf3772c5ebdcd90bf257316c69ba45',
    { from: owner }
  );
  // retrieve leaf
  const response = await t.context.treeFund.getLeaf(
    '0x8fd00f170fdf3772c5ebdcd90bf257316c69ba45',
    { from: owner }
  );

  // get tree state after leaf is added
  const treeStateAfter = await t.context.treeFund.treeState();
  const ESAfter = treeStateAfter['words'][0];

  // created is an integer (unix time)
  t.truthy(Number.isInteger(response['created']['words'][0]));
  // active is set to 'true' when leaf is added
  t.true(response['active']);
  // check tree state has incremented, this is primitive check for now.
  // Eventually should check actual value increase is accurate
  if (logVerbose)
    console.log('T3: ES State Before:', ESBefore, ' ES state after:', ESAfter);
  t.not(ESBefore, ESAfter);

  if (logEvents) logEvent(addedLeaf);
});

// #4 - Here we test that we can getAllLeaves (maybe also validate that response is a checksumed ETH addy?)
// If the response is an empty array the test should fail
test('Successfully retrieved all leaves...', async (t) => {
  const response = await t.context.treeFund.getAllLeaves();
  t.truthy(response[0]);
});

// #5 - Here we test that we can update Leaf creation time
test('Updated leaf created time flag successfully...', async (t) => {
  // retrieve leaf
  const og_created_time = await t.context.treeFund.getLeaf(
    '0xaDaa4fF6319e22DA88bB64Fb2bA2dC5a13f11De4',
    { from: owner }
  );
  await t.context.treeFund.updateLeafCreationTime(
    '0xaDaa4fF6319e22DA88bB64Fb2bA2dC5a13f11De4',
    '39999999',
    { from: owner }
  );
  const new_created_time = await t.context.treeFund.getLeaf(
    '0xaDaa4fF6319e22DA88bB64Fb2bA2dC5a13f11De4',
    { from: owner }
  );
  t.not(
    og_created_time['created']['words'][0],
    new_created_time['created']['words'][0],
    "Created time shouldn't be the same!"
  );
});

// #6 - Here we test that we can update Leaf active flag
test('Updated leaf active flag successfully...', async (t) => {
  // add new leaf, just using random leaf
  await t.context.treeFund.addLeaf(
    '0x829BD824B016326A401d083B33D092293333A830',
    { from: owner }
  );

  // update the leaf
  const updatedLeafActive = await t.context.treeFund.updateLeafActive(
    '0x829BD824B016326A401d083B33D092293333A830',
    false,
    { from: owner }
  );
  // retrieve the leaf
  const response = await t.context.treeFund.getLeaf(
    '0x829BD824B016326A401d083B33D092293333A830',
    { from: owner }
  );
  t.false(response['active'], "Active flag should be 'False'!");
  if (logEvents) logEvent(updatedLeafActive);
});

// #7 - This test is to confirm we can add and update coin mappings
test('Coin mappings can be added, retrieved, & updated...', async (t) => {
  const addCoinMapping = await t.context.treeFund.addCoinMapping(
    'BAT',
    '0x0D8775F648430679A709E98d2b0Cb6250d2887EF',
    { from: owner }
  );
  t.is(
    await t.context.treeFund.getCoinMapping('BAT'),
    '0x0D8775F648430679A709E98d2b0Cb6250d2887EF'
  );
  const updateCoinMapping = await t.context.treeFund.updateCoinMapping(
    'BAT',
    '0xFDE0DbC70A74956D0f51721398Df82119a5BfE03',
    { from: owner }
  );
  // if (logVerbose) console.log(await t.context.treeFund.getCoinMapping("BAT"));
  t.is(
    await t.context.treeFund.getCoinMapping('BAT'),
    '0xFDE0DbC70A74956D0f51721398Df82119a5BfE03'
  );
  if (logEvents) logEvent(addCoinMapping);
  if (logEvents) logEvent(updateCoinMapping);
});

// #8 - This test is to confirm tree state is working for adding new leaves
test('Confirm treeState is adjusted correctly when new leaf is added...', async (t) => {
  const tn = 'T8';
  const treeState = await t.context.treeFund.treeState();
  if (logVerbose) console.log(tn, 'ETState before: ', treeState['words'][0]);

  const treeStateUpdated = await t.context.treeFund.treeStateLastUpdated();
  if (logVerbose)
    console.log(tn, 'ETStateUpdated: ', treeStateUpdated['words'][0]);

  // add a new leaf (random address can be used)
  await t.context.treeFund.addLeaf(
    '0x9C5Dd70D98e9B321217e8232235e25E64E78C595',
    { from: owner }
  );

  const leafIndexLength = await t.context.treeFund.getLeafIndexLength();
  if (logVerbose) console.log(tn, 'leafIndex: ', leafIndexLength['words'][0]);

  const treeState2 = await t.context.treeFund.treeState();
  if (logVerbose) console.log(tn, 'ETState after:', treeState2['words'][0]);
  // divide treeState2 by total number of leafs, -1
  if (logVerbose)
    console.log(
      tn,
      'per leaf:',
      treeState2['words'][0] / (leafIndexLength['words'][0] - 1)
    );

  const getLeaf = await t.context.treeFund.getLeaf(
    '0xaDaa4fF6319e22DA88bB64Fb2bA2dC5a13f11De4',
    { from: owner }
  );
  if (logVerbose)
    console.log(tn, 'leafCreated: ', getLeaf['created']['words'][0]);

  const offset = 5; // necessary for testing as transactions happen too quickly to accurately test time based items
  const treeStateUpdated2 = await t.context.treeFund.treeStateLastUpdated();
  if (logVerbose)
    console.log(tn, 'ETStateUpdated2:', treeStateUpdated2['words'][0] + offset);

  const leafValue =
    treeStateUpdated2['words'][0] + offset - getLeaf['created']['words'][0];
  if (logVerbose)
    console.log(
      tn,
      'leaf 0xaDaa4fF6319e22DA88bB64Fb2bA2dC5a13f11De4 pool value:',
      leafValue
    );

  t.not(leafValue, 0, "Fund/pool value shouldn't be 0...");
});

// #9 More Through tree State Testing
//
test('Explicit tree state testing passed...', async (t) => {
  const tn = 'T9:';

  // First we'll manually get all leaf datas
  const leafCount = await t.context.treeFund.getLeafIndexLength();
  if (logVerbose) console.log(tn, 'leafCount:', leafCount['words'][0]);

  // next we'll get the tree state
  const treeState1 = await t.context.treeFund.treeState();
  if (logVerbose) console.log(tn, 'ES 1:', treeState1['words'][0]);

  // now we'll make sure the tree state is updated & call
  await t.context.treeFund.updateTreeState({ from: owner }).then(await es2());

  // es2
  async function es2() {
    const treeState2 = await t.context.treeFund.treeState();
    if (logVerbose) console.log(tn, 'ES 2:', treeState2['words'][0]);
  }

  const allLeaves = await t.context.treeFund.getAllLeaves();
  //console.log(allLeaves);

  // lets get the leaf creation times
  for (i = 0; i < leafCount; i++) {
    if (logVerbose) {
      const leafData = await t.context.treeFund.getLeaf(allLeaves[i], {
        from: owner,
      });
      console.log(
        tn,
        'leaf:',
        allLeaves[i],
        'created:',
        leafData['created']['words'][0]
      );
    }
    // total leaf
    // total = total + (now - leafData["created"]["words"][0]);
  }

  t.pass();
});

function logEvent(event) {
  // second event is bool
  if (event.logs[0]['args'][1]['words'] === undefined) {
    console.log(
      'EVENT: ',
      event.logs[0]['event'],
      event.logs[0]['args'][0],
      event.logs[0]['args'][1]
    );
    // second event is uint (returned as BigNumber)
  } else {
    console.log(
      'EVENT: ',
      event.logs[0]['event'],
      event.logs[0]['args'][0],
      event.logs[0]['args'][1]['words'][0]
    );
  }
}
