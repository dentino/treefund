// contracts/TreeFund.sol
pragma solidity ^0.6.4;

/// @title An on-chain fund that collects, stores, invests, and distributes coins for benefactors 
/// @author Zachary Wolff @WolfDeFi
/// @notice Seed your tree fund for friends & family  
/// @dev This contract is not production ready

// import contracts
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";


contract TreeFund is Ownable {
    using SafeMath for uint; 
    using SafeMath for uint256; 
    
    // this is a starting point for treeFund config
    // active is config active? only one config should ever be active at a time
    // open - is the fund open for leaf withdrawls default = 0, not open. otherwise set to future unix date
    // max_wd - placeholder for incoming config var 
    struct TreeConf {
        bool active;
        uint open;
        uint max_wd;
    }

    // primitve take at versionable config
    // where uint = config number 
    mapping(uint => TreeConf) private treeFundConf;

    // use this array as index to track all leaves 
    address[] public leafIndex; 
    
    // each leaf node will have an assoiciated struct
    // created = unix time the leafNode joined (or backdated) 
    // active = is the leafNode active or not, inactive nodes cannot withdrawl funds
    // withdrawn (MCD => 1.15) - % of entitlment a given leaf has withdrawn  
    struct Leaf {
        uint created;
        bool active;
        mapping (string => uint) leafWithdraw; 
    }

    // now we map leaf addresses to thier leaf struct
    mapping (address => Leaf) private leafNodes; 

    // create index for funded coins 
    string[] private coinIndex; 

    // mapping for storing funded coins and their contract addresses
    mapping (string => address) private coinAddresses; 

    /// @notice treeState is the source from which we will derive leaf node balances 
    /// treeState is a key tracking mechanism that represents  
    /// the total amount of time (in seconds) that all leaves have been in the fund.  
    uint public treeState; 

    // so we can know when the last time the treeState was updated 
    uint public treeStateLastUpdated; 

    // Events - publicize actions to external listeners
    
    // Base contract events
    event DepositETHLog(uint value);
    event Received(address, uint); 
    event Fallback(address, uint);

    // Leaf Events
    event AddedLeaf(address, uint); 
    event UpdatedLeafActive(address, bool);
    event UpdatedLeafCreation(address, uint);
    event TreeStateUpdated(uint previousTreeState, uint treeState, uint previousTreeStateUpdated, uint treeStateLastUpdated);  
    event GetLeafTree(uint);

    // CoinMapping Events
    event AdddedCoinMapping(address, string);
    event UpdateCoinMapping(address, string);

    // leaf node withdraw events
    event LogLeafWithdraw(uint, uint); 

    // debugging events that will likely be removed 
    event LogBalance(address, uint);
    event Difference(uint difference);
    event UpdatedLeafCreationLog(uint, uint);

    /// @notice run once on deployment 
    constructor() public payable{
        // runs once on contract deployment 

        // initalize Tree State 
        treeStateLastUpdated = now; 

        // add your starting leaf node public addys (min = 1) 
        addLeaf(0x151E3D4efa8906E3D7A52a8498db815BdbcBE05D);
        addLeaf(0x7c12c7C9076c1De898c2eBe35A630ef257fDAE7D);

        // add inital coin mapping for Ethereum token support (required)   
        addCoinMapping("ETH", 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE);

        // add coin mapping for any ERC20 coins you want to fund (optional)
        // for example, as I intented to deposit DAI - mainnet MCD DAI contract address
        // new mappings can be added any time via the admin interface of the contract
        addCoinMapping("MCD", 0x6B175474E89094C44Da98b954EedeAC495271d0F);

        // establish tree fund configuation 
        treeFundConf[0].active = true;
        treeFundConf[0].open = 1590527335; // unix time stamp when fund will open for withdraw 
        treeFundConf[0].max_wd = 50; 

    }

    /// @notice Catches any incoming calls that don't match a function & don't contain Eth
    fallback() external {
    }

    /// @notice Catches any incoming tx's that dont match a function & do contain Eth 
    receive() external payable {
        // check to see if we need to adjust leaf percentages? 
        emit Received(msg.sender, msg.value);
    }  

    /// @notice this function is used to add new leaf nodes 
    function addLeaf(address payable _leaf_node_address) public onlyOwner {
        // Leaf Nodes must be unique. Safe enough (for now) to assume that if the created time is 0 the leaf doesn't yet exist 
        require(leafNodes[_leaf_node_address].created == 0, "Leaf already exists!");
        
        // Set leaf node created time and active flag 
        leafNodes[_leaf_node_address].created = now;
        leafNodes[_leaf_node_address].active = true; 
        // leafNodes[_leaf_node_address].leafWithdraw["ETH"] = 1000; // %/100 for now for two decimal % support  

        // do this before we bump the leafIndex counter        
        updateTreeState();  
        leafIndex.push(_leaf_node_address);
      
        emit AddedLeaf(_leaf_node_address, leafIndex.length);

    }

    /// @notice this admin function is used to backdate leaf node creation time
    /// this allows you to effectively adjust the a given leaf node % of entitlement 
    function updateLeafCreationTime(address _leaf_node_address, uint _created_time) public onlyOwner {
        // used for debugging ES
        emit UpdatedLeafCreationLog(treeStateLastUpdated, leafNodes[_leaf_node_address].created);
        uint previousTreeState = treeState;
        uint previousTreeStateUpdated = treeStateLastUpdated; 
        
        // 1) remove any time that has been added for this leaf 
        // if leaf was last one added and no other functions have trigged update to ES, this will eval to false
        if (treeStateLastUpdated > leafNodes[_leaf_node_address].created) { 
            // how much time has leaf contributed to treeState? 
            uint oldLeafTimeinTotal = now - leafNodes[_leaf_node_address].created;
            // now subtract that ^^ from total pool 
            treeState = treeState - oldLeafTimeinTotal;
        }

        // 2) calc time leaf has been in fund/pool based on new created time and add it to treeState
        uint newLeafTimeinTotal = now - _created_time; 
        /// and add that to the fund treeState
        treeState = treeState + newLeafTimeinTotal;

        // step 3 - as we're going to need to update the treeStateLastUpdated = now; we must also make sure all other leafs get thier time added too
        uint leafCount = leafIndex.length; 

        // how much time has passed since we last updated the treeState? 
        uint difference = now - treeStateLastUpdated; 
        // used for debugging, can remove for prod 
        emit Difference(difference);
        // any leaves that have accrued time since the last update that ARENT the leaf being updated (hence -1), because that's covered with #2 ^^ 
        treeState = treeState + (difference * (leafCount -1));

        // reset our state tracker for total leaf times 
        treeStateLastUpdated = now;

        // finally, update the created time for the leaf 
        leafNodes[_leaf_node_address].created = _created_time;

        // used for debugging, can probably remove in prod 
        emit TreeStateUpdated(previousTreeState, treeState, previousTreeStateUpdated, treeStateLastUpdated);
        emit UpdatedLeafCreation(_leaf_node_address, _created_time);
    }

    /// @notice this function will return the length of the leafIndex array
    function getLeafIndexLength() public view returns (uint) {
        return (leafIndex.length);
    }

    /// @notice this function can be used to update the entitledState
    function updateTreeState() public onlyOwner { 

        // used for debugging 
        uint previousTreeState = treeState;
        uint previousTreeStateUpdated = treeStateLastUpdated; 

        // how much time has passed since we last updated the treeState? 
        uint difference = now - treeStateLastUpdated; 
        // used for debugging, can remove for prod 
        emit Difference(difference);
        // if there is no difference in ESLU, then there is no need to update 
        if (difference != 0) { 
            // add any time that has passed since last update for all leaves! 
            treeState = treeState + (difference * leafIndex.length);
             // used for debugging, can probably remove in prod 
            treeStateLastUpdated = now;
            // used for debugging, can probably remove in prod 
            emit TreeStateUpdated(previousTreeState, treeState, previousTreeStateUpdated, treeStateLastUpdated);
        }


    }

    /// @notice this function is used to update leaf node active flag.
    // the idea here is that an inactive node will not be able to withdraw and/or be considered for % calculations of the fund 
    function updateLeafActive(address _leaf_node_address, bool _active) public onlyOwner {
        leafNodes[_leaf_node_address].active = _active;
        emit UpdatedLeafActive(_leaf_node_address, _active);
    }

    /// @notice this function can be used to retrieve leaf node data 
    function getLeaf(address _leaf_node_address) public view onlyOwner returns(uint created, bool active){ 
        created = leafNodes[_leaf_node_address].created;
        active = leafNodes[_leaf_node_address].active;
    }

    /// @notice used to return all leafnode addresses 
    function getAllLeaves() external view returns (address[] memory) {
        return leafIndex;
    }
    
    /// @notice this function is used to add new funded coin mappings 
    /// 1) add String=>Address mapping | MCD=>0x4242
    /// 2) bump+ index counter to let contract state know there is a new funded coin mapping 
    function addCoinMapping(string memory _coin_name, address payable _contract_address) public onlyOwner {
         // set coin name and mapped address
         coinAddresses[_coin_name] = _contract_address;

         // add new coin to our index, do not remove until we upgrade coin mapping method
         coinIndex.push(_coin_name);
         emit AdddedCoinMapping(_contract_address, _coin_name);
    }

    /// @notice used to return coin mapping. might need to create an index for this too
    function getCoinMapping(string memory _coin_name) public view returns (address){ 
        return coinAddresses[_coin_name];   
    }
    
    /**TypeError: This type is only supported in ABIEncoderV2. Use "pragma experimental ABIEncoderV2;" to enable the feature
    /// @notice used to return all coin names the contract has mapped/stored 
    function getAllcoinMappings() external view returns (string[] memory) {
        return coinIndex;
    }
    */
    
    /// @notice can be used to update a coin name to address mapping, in the event that contract address changes
    function updateCoinMapping (string memory _coin_name, address payable _contract_address) public onlyOwner { 
        coinAddresses[_coin_name] = _contract_address; 
        emit UpdateCoinMapping (_contract_address, _coin_name);
    }

    /// @notice this function is used to calculate & return leaf entitlement %
    // https://stackoverflow.com/questions/42738640/division-in-ethereum-solidity/42739843
    function getLeafTree(address _leaf_node_address) public returns (uint quotient) { 
        // re-entrancy bug/attack? todo: add require() here maybe
        updateTreeState(); // make sure our state is up to date! 

        // much time has leafnode been in the pool/fund
        uint numerator = now - leafNodes[_leaf_node_address].created;
        uint denominator = treeState;  
        uint precision = 4;

        // caution, check safe-to-multiply here
        uint _numerator  = numerator * 10 ** (precision+1);
        // with rounding of last digit
        uint _quotient =  ((_numerator / denominator) + 5) / 10;
        emit GetLeafTree(_quotient);
        return ( _quotient);
    }

    /// @notice This method can be used to deposit ETH into the fund. Likely obsolete at this point  
    function deposit_ETH() public payable { 
        uint value = msg.value;
        emit DepositETHLog(value);
    }

    /// @notice  What is the Ethereum Balance of the contract 
    function getEthBalance() public view returns (uint) {
        address(this).balance;
    }

    /// @notice This function is used to get this contracts balance of a given ERC20 coin 
    function getERC20balance(address _erc_contract_address) public view returns(uint256) {
            IERC20 token = IERC20(_erc_contract_address);
            return (token.balanceOf(address(this)));    
    }

    /// @notice This function is designed for contract owner to transfer ERC20 coins out
    function withdrawERC20(
        address _erc20Contract, 
        address payable recipient, 
        uint256 tokenAmount
        ) public onlyOwner { 
        IERC20 token = IERC20(_erc20Contract);
        token.transfer(recipient, tokenAmount);
    }

    /// @notice This method is used by leaves to withdraw coins from the fund 
    function leafWithdrawRequest(string memory _coin_name, uint256 _tokenAmount) public {
        // step 1 : require msg.sender is a leaf node, revert if not  
        // uint256 leaf_created = leafNodes[msg.sender].created;
        bool leaf_active = leafNodes[msg.sender].active;
        require(leaf_active, "Address is not valid leaf");
          
        // step 2: validate global treeFundConf 
        require(treeFundConf[0].active, "No active config found!");
        require(treeFundConf[0].open < now, "Fund not open for withdrawals!"); // is the fund open for WD?
        
        // step 3: check user specfic withdraw params
        // returns 0 if nothing has been withdrawn 
        uint leaf_limit = leafNodes[msg.sender].leafWithdraw[_coin_name]; // what % have they already withdrawn? 
        uint leaf_percent_entitled = getLeafTree(msg.sender); // this will be the total % of fund they are entitled too
        require(10000 - leaf_limit > 0, "leaf limit % exceeded for this token! v1"); // just to be sure ;)  
        require(leaf_percent_entitled < 10000, "leaf limit % exceeded for this token! v2"); // 10k means leaf has drained this assest! maybe revert Event log this? 

        // step 4: account for withdraw by updating leaf node
        leafNodes[msg.sender].leafWithdraw[_coin_name] = leaf_limit - leaf_percent_entitled;   
        require(leafNodes[msg.sender].leafWithdraw[_coin_name] > leaf_limit, "There was an issue deducting from leaf state!"); // primitive check, needs to be improved

        // step 5: finally, send coins 
        // todo 
        // call withdraw ERC20 work in progress 
        // IERC20 token = IERC20(coinMapping[_coin_name]);
        // token.transfer(recipient, tokenAmount);
    
        // call with ETH...
        // ToDo

        LogLeafWithdraw(leaf_limit, leaf_percent_entitled);
        
        

    }
    
  
}




