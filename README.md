# Welcome to TreeFund

### [A WolfDeFi Project](https://wolfdefi.com)

[![built-with openzeppelin](https://img.shields.io/badge/built%20with-OpenZeppelin-3677FF)](https://docs.openzeppelin.com/)

![TreeFund-UI](./img/TreeFund-UI-1-600.png)

## Overview

TreeFund is an Ethereum DApp in which a grantor can collect, store, invest, and distribute Ether and ERC20 coins to benefactors, or _leaves_. You can think of it like a seed fund for friends, family, or any other cause.

As an example, you could deploy an instance of the TreeFund contract and setup leaf accounts for your family. You can then supply coins to the fund and/or promote public donation addresses to feed tokens into your TreeFund.

~~TreeFund currently supports the ability for the owner to open a DeFi position on [Compound](https://compound.finance/) with the funds DAI. The intent is add support for a host of different DeFi protocols.~~

Leaf benefactors are entitled to funds proportional to how much time a leaf has been active in the fund.

Leaf nodes are represented in the contract as Ethereum addresses. By design, this allows you to set up your TreeFund with user wallet contract addresses from things like [Gnosis Safe](https://safe.gnosis.io/).

With this type of configuration leaf nodes will each have their own mnemonic. A basic treeFundConf system is being implemented to allow flexible configuration patterns to be deployed. In it's most simple form you start with when the fund will ~~open~~ for withdrawals. In another configuration, leaves could withdraw X coins for a one time expense (summer camp) or even on a regular basis.

## Setting Up Your TreeFund

From a high there there are 2 main steps:

1. Configure & deploy your contract to the Ethereum Blockchain (required) - All admin config can be easily done via raw ETH transactions
2. Deploy UI to manage your TreeFund DApp (optional) - Best if you're not comfortable or interested in sending raw transactions and/or you just want to have an nice UI

## 1) Configure & deploy your contract to the Ethereum Blockchain

### Setting Up Your Contract Development Environment

In this guide we will be using OpenZeppelin to deploy and manage our contract. Once things are more dialed in I would like to add info on how you could use Remix or Truffle to deploy and manage the contract.

For the sake of clarity and simplicity, we'll be using [Node Version Manager](https://github.com/nvm-sh/nvm) to install Node but you should be fine with a standard Node install as well.

Base setup:

```bash
$ nvm use node
--> Now using node v12.16.1 (npm v6.13.4)
```

```baseh
$ git clone git@gitlab.com:dentino/treefund.git
$ npm install
```

```bash
$ npm test
```

This will run the command: `npx oz compile && ava --verbose` Contract(s) should complile but tests are a bit of a work in progress right now and it's possible that you might see some failures depending on how things are setup.

### Leaf Setup & Configuration

From a configuration standpoint, leaves can be added when the contract is initally deployed and/or after deployment using the `addLeaf` method. As it stands, all leaves added with the constructor during deployment will have the same `created` timestamp. A given leaves `created` timestamps can be changed with the contract method `updateLeafCreationTime`.

A leaves `created` timestamp is an important part of understanding how the TreeFund works.

### Leaf Setup With Gnosis Safe

[coming soon] -- These instructions will walk you though the process of establishing leaf node wallets using [Gnosis Safe](https://safe.gnosis.io/), then set-up/config and deployment of your own LeafFund contract.

## 2) Deploy UI to manage your TreeFund DApp

**TO DO**

--> MORE SOON. Commands below might not be needed or are still a work in progress

### Helpful commands

Deploy a local Ethereum test chain with Ganache:

```bash
npx ganache-cli --deterministic
```

Of if you're wanting to test with the compound contracts this will fork the Ethereum mainnet and allow you to interact with current mainnet contracts in your local test chain! More detailed info on this command can be found [here](https://medium.com/compound-finance/supplying-assets-to-the-compound-protocol-ec2cf5df5aa)

```bash
ganache-cli  -f https://cloudflare-eth.com/ -m "clutch captain shoe salt awake harvest setup primary inmate ugly among become" -u 0x9759A6Ac90977b93B58547b4A71c78317f391A28 -i 1000
```

The `-u 0x97..` flag is used to `unlock` that address on our local testnet which allows us to mint testnet DAI.

### Dependency Notes -

Helps to understand why a given dependency was installed and how it's used in the project

#### @truffle/hdwallet-provider

`npm install --save-dev @truffle/hdwallet-provider`
Used to sign all our transactions locally.

[More on why hdwallet-provider is being used here](https://docs.openzeppelin.com/learn/connecting-to-public-test-networks#creating-a-new-account)

####[please see TreeFund admin guide](ADMIN.md)
