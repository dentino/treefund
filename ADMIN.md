# Welcome to TreeFund Admin Guide

### [A WolfDeFi Project](https://wolfdefi.com)

[![built-with openzeppelin](https://img.shields.io/badge/built%20with-OpenZeppelin-3677FF)](https://docs.openzeppelin.com/)

## Overview

Here is a list of commands that will get you started executing the most common admin functions for the TreeFund. TreeFund is built to be malleable and adaptable. There is are admin controls to the application and important parameters and rules can be adjusted by the contract owner at anytime. While that could change, for now it's a feature.

Add new leaf node:

```
npx oz send-tx --to 0xe78A0F7E598Cc8b0Bb87894B0F60dD2a88d6a8Ab --method addLeaf --args 0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1
```

Simulate leaf wd request (in dev env):

```
npx oz send-tx --to 0xe78A0F7E598Cc8b0Bb87894B0F60dD2a88d6a8Ab --method leafWithdrawRequest --from 0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1 --args ETH,10
```

random commands:

```
npx oz send-tx --to 0xb65a998b2EEee36810df23eF3Ebad3eF732b2812 --value 1000000000000000 --method deposit_ETH
npx oz transfer -v --to 0xb65a998b2EEee36810df23eF3Ebad3eF732b2812 --value 10000000000000000 --unit wei
npx oz call --to 0xb65a998b2EEee36810df23eF3Ebad3eF732b2812 --method balance
npx oz call --to 0xb65a998b2EEee36810df23eF3Ebad3eF732b2812 --method any_balance --args 0x445305B503E468B9635DfFE214358bb67499Ed5a
npx oz balance 0xb65a998b2EEee36810df23eF3Ebad3eF732b2812

```

even better, here we set local Bash ENVs which allows us to execute commands more fluidly.

```

export CADDY="0x6468B63629b6097d190D94f3fE80De39C136a501"
export LEAF_1_ADDY="0x8EdE8a30D2c83B227f5f415AEA0C5Cc7ca2D2f11"
export LEAF_2_ADDY="0x445305B503E468B9635DfFE214358bb67499Ed5a"
export LEAF_3_ADDY="0xaDaa4fF6319e22DA88bB64Fb2bA2dC5a13f11De4"

npx oz send-tx --to $CADDY --value 1000000000000000 --method deposit_ETH
npx oz transfer -v --to $CADDY --value 10000000000000000 --unit wei
npx oz balance $CADDY
npx oz call --to $CADDY --method any_balance --args $LEAF_2_ADDY
npx oz transfer -v --to $CADDY --value .01 --unit ether

```
